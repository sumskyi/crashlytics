require_relative '../lib/config'

describe Ini::Config do
  let(:ini_path) do
    File.expand_path('../spec/fixtures/sample.ini', File.dirname(__FILE__))
  end
  subject { Ini::Config.new(ini_path, overrides).run }
  let(:overrides) { [] }

  context 'empty overrides' do
    let(:ftp) do
      { name: 'hello there, ftp uploading',
        path: '/tmp/',
        enabled: false }
    end

    it 'ok' do
      expect(subject.ftp3.name).to eq nil
      expect(subject.ftp).to eq ftp
      expect(subject.ftp.name).to eq 'hello there, ftp uploading'
      expect(subject.ftp.name_not_exists).to eq nil
    end
  end

  context '1 override' do
    let(:overrides) { ['ubuntu'] }

    let(:ftp) do
      { name: 'hello there, ftp uploading',
        path: '/etc/var/uploads',
        enabled: false }
    end

    it 'ok' do
      expect(subject.ftp3.name).to eq nil
      expect(subject.ftp).to eq ftp
      expect(subject.ftp.name).to eq 'hello there, ftp uploading'
      expect(subject.ftp.name_not_exists).to eq nil
    end
  end

  context '2 overrides' do
    let(:overrides) { ['ubuntu', :production] }

    let(:ftp) do
      { name: 'hello there, ftp uploading',
        path: '/etc/var/uploads',
        enabled: false }
    end

    it 'ok' do
      expect(subject.ftp3.name).to eq nil
      expect(subject.ftp).to eq ftp
      expect(subject.ftp.name).to eq 'hello there, ftp uploading'
      expect(subject.ftp.name_not_exists).to eq nil
    end
  end
end

describe Object do
  let(:ini_path) do
    File.expand_path('../spec/fixtures/sample.ini', File.dirname(__FILE__))
  end
  let(:config) { load_config(ini_path, ['ubuntu', :production]) }

  describe '#load_config' do
    specify { expect(config.common.paid_users_size_limit).to eq 2_147_483_648 }
    specify { expect(config.ftp.name).to eq 'hello there, ftp uploading' }
    specify { expect(config.http.params).to eq %w(array of values) }
    specify { expect(config.ftp.lastname).to eq nil }
    specify { expect(config.ftp.enabled).to eq false }
    specify { expect(config.ftp[:path]).to eq '/etc/var/uploads' }
    specify do
      expect(config.ftp).to eq(name: 'hello there, ftp uploading',
                               path: '/etc/var/uploads',
                               enabled: false)
    end
  end
end
