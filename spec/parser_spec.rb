require 'parslet/rig/rspec'
require_relative '../lib/parser'

describe Ini::Parser do
  let(:ini_parser) { Ini::Parser.new }

  context 'key_group parsing' do
    let(:parser) { ini_parser.key_group }

    it 'parses key group' do
      expect(parser).to parse('[qwerty]')
    end
  end

  context 'key parsing' do
    let(:parser) { ini_parser.key }

    it 'parses strings' do
      expect(parser).to parse('qwerty')
    end
  end

  context 'value parsing' do
    let(:parser) { ini_parser.value }

    it 'parses integers' do
      expect(parser).to parse('1')
      expect(parser).to parse('-123')
      expect(parser).to parse('181')
      expect(parser).to_not parse('0181')
    end

    it 'parses booleans' do
      expect(parser).to parse('true')
      expect(parser).to parse('false')
      expect(parser).to parse('1')
      expect(parser).to parse('0')
      expect(parser).to parse('yes')
      expect(parser).to parse('no')
      expect(parser).to_not parse('truefalse')

      expect(parser.parse('true')).to eq boolean: 'true'
      expect(parser.parse('false')).to eq boolean: 'false'
    end

    it 'parses paths' do
      expect(parser).to parse('/')
      expect(parser).to parse('/etc')
      expect(parser).to parse('/etc/var/uploads')
      expect(parser).to parse('/srv/var/tmp/')
      expect(parser).to_not parse('/srv/v#$%^ar/tmp/')
    end

    it 'parses arrays of values' do
      expect(parser).to parse('1,2,3')
      expect(parser).to parse('array,of,values')
    end
  end # value

  context 'assignment parsing' do
    let(:parser) { ini_parser.assignment }

    it 'parses value' do
      expect(parser).to parse('a = 12345678')
      expect(parser).to parse('a = 12345678 ; comment there')
    end

    it 'parses environment' do
      expect(parser).to parse('qwerty<development> = 234565')
    end
  end

  context 'assignments parsing' do
    let(:parser) { ini_parser.assignments }

    it 'parses value' do
      expect(parser).to parse("a = 12345678\nb=98765432")
      expect(parser).to parse("a = 12345678 ; comment there\ne=76543")
    end
  end

  context 'key_group parsing' do
    let(:parser) { ini_parser.key_group }

    it 'parses value' do
      expect(parser).to parse("[abc]\na=3")
    end
  end

  context 'comment' do
    let(:parser) { ini_parser.comment }

    it 'parses comment' do
      expect(parser).to parse(';the whole line comment')
      expect(parser).to parse('; the whole line comment')
    end
  end

  context 'real example' do
    let(:result) { Ini::Parser.new.parse(sample) }

    context 'provided ini file' do
      let(:sample) { File.read('spec/fixtures/sample.ini') }
      it 'parses ini string' do
        expect(result[:document][0][:key_group][0][:group_name]).to eq 'common'
      end
    end

    context 'step by step' do
      let(:sample) do
        "\n[abcd]\na = 234567\n"
      end

      it 'key group & assignment' do
        expect(result[:document][0][:key_group][0][:group_name]).to match 'abcd'
      end
    end
  end
end
