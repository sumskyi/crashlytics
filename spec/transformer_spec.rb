require 'parslet/rig/rspec'
require_relative '../lib/transformer'

describe Ini::Transformer do
  let(:xform) { Ini::Transformer.new }

  context 'integer' do
    it 'transforms' do
      expect(xform.apply(integer: '123')).to eq(123)
      expect(xform.apply(integer: '-123')).to eq(-123)
    end
  end

  context 'boolean' do
    it 'transforms an 0 and 1 to boolean' do
      expect(xform.apply(integer: '1')).to eq(true)
      expect(xform.apply(integer: '0')).to eq(false)

      expect(xform.apply(boolean: 'yes')).to eq(true)
      expect(xform.apply(boolean: 'no')).to eq(false)

      expect(xform.apply(boolean: 'true')).to eq(true)
      expect(xform.apply(boolean: 'false')).to eq(false)
    end
  end

  context 'environment' do
    it 'transforms nil to default' do
      expect(xform.apply(env: nil)).to eq(:default)
    end

    it 'transforms env value' do
      env = { env_name: 'staging' }

      expect(xform.apply(env: env)).to eq(:staging)
    end
  end

  context 'array' do
    let(:value) { 'array,of,values' }
    let(:expected) { %w(array of values) }

    it 'transforms nil to default' do
      expect(xform.apply(array: value)).to eq(expected)
    end
  end

  context 'path' do
    let(:value) { '/var/www' }

    it 'transforms path to string' do
      expect(xform.apply(path: value)).to eq(value)
    end
  end

  context 'string' do
    let(:value) { 'http uploading' }

    it 'transforms path to string' do
      expect(xform.apply(string: value)).to eq(value)
    end
  end
end
