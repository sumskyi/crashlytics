require 'parslet'

module Ini
  # Transforming rules
  class Transformer < Parslet::Transform
    rule(integer: simple(:n)) do
      int = Integer(n)
      case int
      when 1
        true
      when 0
        false
      else
        int
      end
    end

    rule(boolean: simple(:b)) do
      case b
      when 'yes', 'true'
        true
      when 'no', 'false'
        false
      end
    end

    rule(env: subtree(:e)) do
      if e.nil?
        :default
      else
        e[:env_name].to_sym
      end
    end

    rule(array: simple(:a)) do
      a.to_s.split(',')
    end

    rule(path: simple(:path)) do
      path.to_s
    end

    rule(string: simple(:s)) do
      s.to_s
    end

    rule(group_name: simple(:group_name)) do
      { group_name: group_name.to_s }
    end

    rule(key: simple(:key), env: subtree(:env), value: subtree(:value)) do
      new_env = env.nil? ? :default : env[:env_name].to_sym
      { key: key.to_s, env: new_env, value: value }
    end
  end
end
