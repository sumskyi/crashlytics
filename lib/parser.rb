require 'parslet'

module Ini
  # PEG Parsing rules
  class Parser < Parslet::Parser
    rule(:digit) { match('[0-9]') }
    rule(:char) { match('[a-z0-9_]') }
    rule(:newline) { str("\n") >> str("\r").maybe }
    rule(:quote) { str('"') }

    rule(:boolean) do
      (str('true') |
       str('false') |
       str('yes') |
       str('no') |
       str('1') |
       str('0')).as(:boolean)
    end

    rule(:integer) do
      (str('-').maybe >> match('[1-9]') >> digit.repeat).as(:integer)
    end

    rule(:string_special) { match['\0\t\n\r"\\\\'] }
    rule(:escaped_special) { str('\\') >> match['0tnr"\\\\'] }
    rule(:string) do
      quote >>
        (escaped_special | string_special.absent? >> any).repeat.as(:string) >>
        quote
    end

    rule(:path) do
      ((str('/') >> char.repeat).repeat).as(:path)
    end

    rule(:value) do
      integer | boolean | string | array | path
    end

    rule :array do
      (char.repeat >> (str(',') >> char.repeat).repeat(1)).as(:array)
    end

    rule(:space) { match["\t "] }
    rule(:whitespace) { space.repeat }

    rule :key do
      (match['\\[\\]='].absent? >> space.absent? >> char).repeat(1)
    end

    rule :env do
      str('<') >> char.repeat.as(:env_name) >> str('>')
    end

    rule :assignment do
      key.as(:key) >>
        env.maybe.as(:env) >>
        whitespace >> str('=') >> whitespace >> value.as(:value) >>
        whitespace >> comment.maybe
    end
    rule :assignments do
      (assignment >>
       (newline >> (assignment | whitespace >> comment.maybe)).repeat)
        .as(:assignments)
    end

    rule :group_name do
      str('[') >> (str(']').absent? >> any).repeat(1)
        .as(:group_name) >> str(']') >> whitespace >> comment.maybe
    end

    rule :key_group do
      (group_name >>
       (newline >> (assignment | whitespace >> comment.maybe))
         .repeat).as(:key_group)
    end

    rule(:semicolon) { str(';') }
    rule(:comment) do
      assignment.maybe >> semicolon >> (newline.absent? >> any).repeat(1)
    end

    rule(:empty_lines) do
      (whitespace >> comment.maybe >> newline).repeat
    end

    rule :document do
      (empty_lines >> key_group.repeat >> newline.maybe).as(:document)
    end

    root :document

    def self.parse(str)
      new.parse str
    end
  end
end
