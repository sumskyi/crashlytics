require_relative './parser'
require_relative './transformer'

module Ini
  # Configuration deep access class
  class Config < BasicObject
    def initialize(path, overrides)
      @path = path

      # reverse for choosing right priority:
      # the first override has more weight than others
      @overrides = overrides.map(&:to_sym).uniq.reverse
      @container = ::Class.new
    end

    def run
      parser[:document].each do |group|
        # [{:group_name: "ftp"},
        #  {:key => "basic_size_limit", :env => :default, :value => 26214400},
        #  {:key => ...,                :env => ...,      :value => ... }
        # ]
        key_group = group[:key_group]

        # "ftp"
        group_name = key_group.find { |hash| hash[:group_name] }[:group_name]

        # select key => value pairs list
        keys_values_all = key_group.select { |hash| hash[:key] }

        # { :default => {...}, :production => {...} }
        keys_values = map_values(keys_values_all)

        # @ftp = { :default => {...}, :production => {...} }
        @container.instance_variable_set("@#{group_name}", keys_values)
      end

      self
    end

    private

    # { :default =>    {
    #     :name => "hello there, ftp uploading",
    #     :path => "/tmp/",
    #     :enabled => false },
    #   :production => {:path=> "/srv/var/tmp/"},
    #   :staging =>    {:path=> "/srv/uploads/"},
    #   :ubuntu =>     {:path=> "/etc/var/uploads"}
    # }
    def map_values(keys_values_all)
      keys_values_all.each_with_object({}) do |el, memo|
        key = el[:key].to_sym
        env = el[:env]
        val = el[:value]

        memo[env] ||= {}
        memo[env][key] = val
      end
    end

    def method_missing(method, *_args, &_block)
      key_group = @container.instance_variable_get("@#{method}")
      return {}.extend(ExtendedAccess) unless key_group

      @overrides.each_with_object(key_group[:default]) do |override_name, memo|
        high_priority = key_group[override_name]
        memo.merge!(high_priority) if high_priority
      end.extend(ExtendedAccess)
    end

    def source
      ::File.read(@path)
    end

    def parser
      @_parser ||= ::Ini::Transformer.new.apply(
        ::Ini::Parser.new.parse(source)
      )
    end
  end

  # extension for Hash for accessing keys by method call
  module ExtendedAccess
    def method_missing(method, *_args, &_block)
      self[method]
    end
  end
end

# re-opening Object to add load_config method
class Object
  def load_config(path, overrides = [])
    @_config ||= Ini::Config.new(path, overrides).run
  end
end
